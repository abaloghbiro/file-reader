package hu.braininghub.bh06.ooptraining.filereader;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class App {

	public static void main(String[] args) {

		if (args.length > 0) {
			String filename = args[0];
			try {
				List<String> lines = FileUtils.readLines(new File(filename), "UTF-8");

				for (String l : lines) {
					System.out.println("Line: " + l);
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Provide filename parameter!");
		}

	}

}
